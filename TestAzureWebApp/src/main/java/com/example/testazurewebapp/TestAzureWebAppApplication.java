package com.example.testazurewebapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestAzureWebAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestAzureWebAppApplication.class, args);
    }

}
