package com.example.testazurewebapp.service;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class AzureWebAppController {

    @GetMapping("/message")
    public String getMessage(){

        return "Hello World";
    }

    @GetMapping("/message/{userName}")
    public String getMessageByUserInPath(@PathVariable String userName){

        return "hello world for "+userName;
    }
}
